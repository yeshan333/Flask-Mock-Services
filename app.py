
from flask import Flask, request, abort, jsonify
from flask_cors import CORS, cross_origin

app = Flask(__name__)

CORS(app)

cors = CORS(app, resources={r"/*": {"origin": "*"}})


@app.route('/api/login/account', methods=['GET', 'POST'])
@cross_origin()
def login():
    print(request.headers)
    if not request.json or not 'userName' in request.json \
            or not 'password' in request.json:
        abort(400)

    if request.json['userName'] == 'admin':
        # 用户登录成功响应
        result = {
            'status': 'ok',
            'type': 'account',          # 登录类型：账号 username、密码 password
            'currentAuthority': 'admin'  # 权限级别 user or admin or guest（无访问权限）
        }
    else:
        # 用户登录成功响应
        result = {
            'status': 'ok',
            'type': 'account',          # 登录类型：账号 username、密码 password
            'currentAuthority': 'user'  # 权限级别 user or admin or guest（无访问权限）
        }

    """
    # 用户登录失败响应

    result = {
        'status': 'error',
        'type': 'account',           # 登录类型：账号 username、密码 password
        'currentAuthority': 'guest'  # 权限级别 user or admin or guest（无访问权限）
    }

    # 注册请求，json，权限默认为 user
    register = {
        'ID': 'type number',         # 身份证号
        'Username': 'xxx',           # 姓名
        'Password': 'hash code',     # 密码
        'Gender': 'F or M',          # 性别，F: Female, M: Male
        'Age': 'type number',        # 年龄
    }
    """

    return jsonify(result)


@app.route('/api/currentUser')
def currentuser():
    result = {
        "name": "Serati Ma",
        "avatar": "https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png",
        "userid": "00000001",
        "email": "antdesign@alipay.com",
        "signature": "海纳百川，有容乃大",
        "title": "交互专家",
        "group": "蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED",
        "tags": [
            {
                "key": "0",
                "label": "很有想法的"
            },
            {
                "key": "1",
                "label": "专注设计"
            },
            {
                "key": "2",
                "label": "辣~"
            },
            {
                "key": "3",
                "label": "大长腿"
            },
            {
                "key": "4",
                "label": "川妹子"
            },
            {
                "key": "5",
                "label": "海纳百川"
            }],
        "notifyCount": 12,
        "unreadCount": 11,
        "country": "China",
        "geographic": {
            "province": {
                "label": "浙江省",
                "key": "330000"
            },
            "city": {
                "label": "杭州市",
                "key": "330100"
            }
        },
        "address": "西湖区工专路 77 号",
        "phone": "0752-268888888"
    }

    return jsonify(result)


@app.route('/api/users')
def users():
    result = {
        'key': '1',
        'name': 'John Brown',
        'age': 32,
        'address': 'New York No. 1 Lake Park',
    },
    {
        'key': '2',
        'name': 'Jim Green',
        'age': 42,
        'address': 'London No. 1 Lake Park',
    },
    {
        'key': '3',
        'name': 'Joe Black',
        'age': 32,
        'address': 'Sidney No. 1 Lake Park',
    }
    return jsonify(result)


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=8080,debug=True)
